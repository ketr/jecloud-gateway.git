package com.je.gateway.filter;

import org.apache.servicecomb.common.rest.RestConst;
import org.apache.servicecomb.core.Invocation;
import org.apache.servicecomb.foundation.vertx.http.HttpServletRequestEx;
import org.apache.servicecomb.swagger.invocation.Response;

public class GatewayBuildHandlerContextFilter extends AbstractHttpServerFilter {


    public GatewayBuildHandlerContextFilter() {
    }

    @Override
    public int getOrder() {
        return 2;
    }

    @Override
    public Response afterReceiveRequest(Invocation invocation, HttpServletRequestEx requestEx) {
        //指定path，不将参数拼到url后面，避免请求头过大
        if (requestEx.getMethod().equals("GET") || requestEx.getMethod().equals("get")) {
            return null;
        }
        invocation.getHandlerContext().put(RestConst.REST_CLIENT_REQUEST_PATH, requestEx.getRequestURI());
        return null;
    }


}
