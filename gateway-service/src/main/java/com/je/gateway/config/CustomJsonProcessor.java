package com.je.gateway.config;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.je.gateway.serializer.BigDecimalToStringSerializer;
import com.je.gateway.serializer.LongToStringSerializer;
import org.apache.servicecomb.common.rest.codec.produce.ProduceProcessor;
import org.springframework.http.MediaType;

import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;

public class CustomJsonProcessor implements ProduceProcessor {
    private final ObjectMapper objectMapper;

    public CustomJsonProcessor() {
        this.objectMapper = new ObjectMapper();
        // 注册自定义序列化器
        SimpleModule module = new SimpleModule();
        module.addSerializer(BigDecimal.class, new BigDecimalToStringSerializer());
        module.addSerializer(Long.class, new LongToStringSerializer());
        this.objectMapper.registerModule(module);
    }

    @Override
    public String getName() {
        return MediaType.APPLICATION_JSON_VALUE;
    }

    @Override
    public int getOrder() {
        return -1; // 优先级高于默认的 JSON 处理器
    }

    @Override
    public void doEncodeResponse(OutputStream output, Object result) throws Exception {
        objectMapper.writeValue(output, result);
    }

    @Override
    public Object doDecodeResponse(InputStream input, JavaType type) throws Exception {
        return objectMapper.readValue(input, type);
    }
}

