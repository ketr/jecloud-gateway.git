package com.je.gateway.security;

public interface PlatformInputSecurityHandler<T> {

    /**
     * 处理平台安全问题
     * @param name
     * @param value
     * @return
     */
    Boolean valid(String name,T value);
}
