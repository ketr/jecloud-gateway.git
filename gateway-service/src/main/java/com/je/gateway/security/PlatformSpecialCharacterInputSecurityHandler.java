package com.je.gateway.security;

import java.util.regex.Pattern;

public class PlatformSpecialCharacterInputSecurityHandler implements  PlatformInputSecurityHandler<String>{

    Pattern pattern= Pattern.compile("\\b((select.*and)|asc|exec|insert|select|drop|grant|alter|delete|update|count|chr|mid|master|truncate|char|declare|(select.*or))\\b|(\\*|;|\\+|'|%)");

    @Override
    public Boolean valid(String name, String value) {
        if(value==null){
            return false;
        }
        return  pattern.matcher(value.toString()).find();
    }
}
