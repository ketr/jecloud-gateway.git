package com.je.gateway.security;

import com.google.common.base.Strings;
import com.je.gateway.util.FileNameValidator;
import com.je.gateway.util.RedisUtil;
import io.vertx.ext.web.FileUpload;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.servicecomb.swagger.invocation.InvocationType;
import org.apache.servicecomb.swagger.invocation.Response;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.IntStream;

public class PlatformInputSecurityValidatorFactory {

    public static boolean validatorSpecialCharacter(String name, String value) {
        PlatformInputSecurityHandler platformInputSecurityHandler = new PlatformSpecialCharacterInputSecurityHandler();
        return platformInputSecurityHandler.valid(name, value);
    }

    public static boolean validScript(String name, String value) {
        PlatformInputSecurityHandler platformInputSecurityHandler = new PlatformScriptInputSecurityHandler();
        return platformInputSecurityHandler.valid(name, value);
    }

    public static Response validatorCharacter(String name, String value) {
        //是否开启防SQL注入
        Object sqlInjectionValue = RedisUtil.getHash("systemDecryptCache", "OPEN_ANTI_SQL_INJECTION");
        String isOpenSqlInjection = sqlInjectionValue == null ? "" : sqlInjectionValue.toString();
        if ("1".equals(isOpenSqlInjection) && validatorSpecialCharacter(name, value)) {
            return Response.createFail(InvocationType.PRODUCER, String.format("字段【%s】的值【%s】存在SQL注入风险，请排查！", name, value));
        }
        //是否开启防XSS攻击
        Object xxlAttackValue = RedisUtil.getHash("systemDecryptCache", "OPEN_ANTI_XSS_ATTACK");
        String isOpenXxlAttack = xxlAttackValue == null ? "" : xxlAttackValue.toString();
        if ("1".equals(isOpenXxlAttack) && validScript(name, value)) {
            return Response.createFail(InvocationType.PRODUCER, String.format("字段【%s】的值【%s】存在XSS攻击风险，请排查！", name, value));
        }
        return null;
    }

    public static Response validatorFileUploads(List<FileUpload> fileUploads) {
        Object xxlAttackValue = RedisUtil.getHash("systemDecryptCache", "OPEN_ANTI_XSS_ATTACK");
        String isOpenXxlAttack = xxlAttackValue == null ? "" : xxlAttackValue.toString();
        if ("1".equals(isOpenXxlAttack) && fileUploads.size() > 0) {
            AtomicReference<String> errorMeg = new AtomicReference<>("");
            fileUploads.forEach(fileUpload -> {
                String uploadedFileName = fileUpload.fileName();
                //先校验文件名称是否合法
                if (!FileNameValidator.isValidFileName(uploadedFileName)) {
                    errorMeg.set(String.format("文件名称【%s】存在XSS攻击风险，请排查！", uploadedFileName));
                    return;
                }

                String uploadedFileMimeType = fileUpload.contentType();
                if ("application/pdf".toUpperCase().equals(uploadedFileMimeType.toUpperCase())) {
                    String uploadedTempFilePath = fileUpload.uploadedFileName();
                    File file = new File(uploadedTempFilePath);

                    try (PDDocument document = PDDocument.load(file)) {
                        boolean haveJavaScript = containsJavaScript(document);
                        if (haveJavaScript) {
                            errorMeg.set(String.format("文件【%s】存在XSS攻击风险，请排查！", uploadedFileName));
                            return;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        errorMeg.set(String.format("文件【%s】处理失败，请检查文件格式！", uploadedFileName));
                        return;
                    }
                }
            });
            if (!Strings.isNullOrEmpty(errorMeg.get())) {
                return Response.createFail(InvocationType.PRODUCER, errorMeg.get());
            }
        }

        return null;
    }


    private static boolean containsJavaScript(PDDocument document) {
        if (document.getDocument().getTrailer().toString().contains("COSName{JS}")
                || document.getDocument().getTrailer().toString().contains("COSName{JavaScript}")) {
            return true;
        }
        PDPageTree pages = document.getPages();
        return IntStream.range(0, pages.getCount()).anyMatch(i -> {
            String pageContent = pages.get(i).getCOSObject().toString();
            return pageContent.contains("COSName{JS}") || pageContent.contains("COSName{JavaScript}");
        });
    }
}

