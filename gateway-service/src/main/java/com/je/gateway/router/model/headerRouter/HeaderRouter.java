package com.je.gateway.router.model.headerRouter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * 请求头路由实体类
 */
public class HeaderRouter implements Serializable {

    private String path = "/(.*)";
    private Pattern pattern = Pattern.compile(path);
    private boolean enable = false;

    private List<HeaderUrl> headerUrls = new ArrayList<>();

    public HeaderRouter() {
    }

    public HeaderRouter(String pattern) {
        this.pattern = Pattern.compile(pattern);
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
        this.pattern = Pattern.compile(path);
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public List<HeaderUrl> getHeaderUrls() {
        return headerUrls;
    }

    public void addUrl(HeaderUrl headerUrl){
        this.headerUrls.add(headerUrl);
    }

    public void removeUrl(HeaderUrl headerUrl){
        this.headerUrls.remove(headerUrl);
    }

}
