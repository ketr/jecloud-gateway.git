package com.je.gateway.router.model.paramRouter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class ParamUrl implements Serializable {

    public static final String VALUE_TYPE_STRING = "string";

    public static final String VALUE_TYPE_ARRAY = "array";

    private String path;

    /**
     * 要真实转发的url，此优先级要高于router计算的url
     */
    private String realPath;

    private String valueType = VALUE_TYPE_STRING;

    private String splitter;

    private String valueKey;

    private Pattern pattern;

    private String paramKey;

    private boolean substr;

    private String subChar;

    private int subIndex;

    private String microServiceName;

    private String versionRule;

    private int prefixSegmentCount;

    private boolean paramRef;

    private transient String routerValue;

    private List<String> paramValues = new ArrayList<>();

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
        this.pattern = Pattern.compile(path);
    }

    public boolean isSubstr() {
        return substr;
    }

    public void setSubstr(boolean substr) {
        this.substr = substr;
    }

    public String getSubChar() {
        return subChar;
    }

    public void setSubChar(String subChar) {
        this.subChar = subChar;
    }

    public int getSubIndex() {
        return subIndex;
    }

    public void setSubIndex(int subIndex) {
        this.subIndex = subIndex;
    }

    public String getRealPath() {
        return realPath;
    }

    public void setRealPath(String realPath) {
        this.realPath = realPath;
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    public String getValueKey() {
        return valueKey;
    }

    public void setValueKey(String valueKey) {
        this.valueKey = valueKey;
    }

    public String getSplitter() {
        return splitter;
    }

    public void setSplitter(String splitter) {
        this.splitter = splitter;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public String getParamKey() {
        return paramKey;
    }

    public void setParamKey(String paramKey) {
        this.paramKey = paramKey;
    }

    public List<String> getParamValues() {
        return paramValues;
    }

    public void setParamValues(List<String> paramValues) {
        this.paramValues = paramValues;
    }

    public String getMicroServiceName() {
        return microServiceName;
    }

    public void setMicroServiceName(String microServiceName) {
        this.microServiceName = microServiceName;
    }

    public String getVersionRule() {
        return versionRule;
    }

    public void setVersionRule(String versionRule) {
        this.versionRule = versionRule;
    }

    public int getPrefixSegmentCount() {
        return prefixSegmentCount;
    }

    public void setPrefixSegmentCount(int prefixSegmentCount) {
        this.prefixSegmentCount = prefixSegmentCount;
    }

    public boolean isParamRef() {
        return paramRef;
    }

    public void setParamRef(boolean paramRef) {
        this.paramRef = paramRef;
    }

    public void addParamValue(String value){
        this.paramValues.add(value);
    }

    public void removeParamValue(String value){
        this.paramValues.remove(value);
    }

    public String getRouterValue() {
        return routerValue;
    }

    public void setRouterValue(String routerValue) {
        this.routerValue = routerValue;
    }
}
