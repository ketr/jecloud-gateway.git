package com.je.gateway.router.loader;

import com.google.common.base.Strings;
import com.je.gateway.router.model.headerRouter.HeaderRouter;
import com.je.gateway.router.model.headerRouter.HeaderUrl;
import com.je.gateway.router.GlobalRouterParams;
import com.je.gateway.router.model.paramValue.ConfigParamValues;
import com.je.gateway.xml.AbstractXmlConfigResourceLoader;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 请求头路由XML配置加载器
 */
public class UrlHeaderRouterResourceLoader extends AbstractXmlConfigResourceLoader<HeaderRouter> {

    private static final Logger logger = LoggerFactory.getLogger(UrlHeaderRouterResourceLoader.class);

    private static final String DEFAULT_CONFIG_PAHT = "headerRouter.xml";

    /**
     * pattern属性标签
     */
    private static final String ROUTER_PATTERN_ATTRIBUTE_KEY = "pattern";
    /**
     * enable属性标签
     */
    private static final String ROUTER_ENABLE_ATTRIBUTE_KEY = "enable";
    /**
     * suffix microServiceName属性
     */
    private static final String ROUTER_PATTERN_HEADERURL_PATH_KEY = "path";
    /**
     * suffix realPath属性
     */
    private static final String ROUTER_PATTERN_HEADERURL_REALPATH_KEY = "realPath";
    /**
     * suffix microServiceName属性
     */
    private static final String ROUTER_PATTERN_HEADERURL_HEADERKEY_KEY = "headerKey";
    /**
     * suffix microServiceName属性
     */
    private static final String ROUTER_PATTERN_HEADERURL_MICROSERVICENAME_KEY = "microServiceName";
    /**
     * suffix versionRule属性
     */
    private static final String ROUTER_PATTERN_HEADERURL_VERSIONRULE_KEY = "versionRule";
    /**
     * suffix prefixSegmentCount属性
     */
    private static final String ROUTER_PATTERN_HEADERURL_PREFIXSEGEMENTCOUNT_KEY = "prefixSegmentCount";

    public UrlHeaderRouterResourceLoader() {
        this(DEFAULT_CONFIG_PAHT);
    }

    public UrlHeaderRouterResourceLoader(String resourcePath) {
        super(resourcePath);
    }

    @Override
    public HeaderRouter parse(Element rootElement) {
        //获取pattern配置
        String pattern = rootElement.attributeValue(ROUTER_PATTERN_ATTRIBUTE_KEY);
        String enable = rootElement.attributeValue(ROUTER_ENABLE_ATTRIBUTE_KEY);

        HeaderRouter headerRouter;
        if(Strings.isNullOrEmpty(pattern)){
            headerRouter = new HeaderRouter();
        }else{
            headerRouter = new HeaderRouter(pattern);
        }

        if(!Strings.isNullOrEmpty(enable)){
            if("true".equals(enable)){
                headerRouter.setEnable(true);
            }else{
                headerRouter.setEnable(false);
            }
        }

        List<Element> elements = rootElement.elements();
        if(elements == null || elements.isEmpty()){
            return headerRouter;
        }

        for (Element eachElement: elements) {
            headerRouter.addUrl(parseHeaderUrl(eachElement));
        }
        return headerRouter;
    }

    private HeaderUrl parseHeaderUrl(Element headUrlElement){
        HeaderUrl headerUrl = new HeaderUrl();
        String path = headUrlElement.attributeValue(ROUTER_PATTERN_HEADERURL_PATH_KEY);
        String realPath = headUrlElement.attributeValue(ROUTER_PATTERN_HEADERURL_REALPATH_KEY);
        String headerKey = headUrlElement.attributeValue(ROUTER_PATTERN_HEADERURL_HEADERKEY_KEY);
        String microServiceName = headUrlElement.attributeValue(ROUTER_PATTERN_HEADERURL_MICROSERVICENAME_KEY);
        String versionRule = headUrlElement.attributeValue(ROUTER_PATTERN_HEADERURL_VERSIONRULE_KEY);
        String prefixSegementCount = headUrlElement.attributeValue(ROUTER_PATTERN_HEADERURL_PREFIXSEGEMENTCOUNT_KEY);
        if(Strings.isNullOrEmpty(path) || Strings.isNullOrEmpty(microServiceName) || Strings.isNullOrEmpty(headerKey)
                || Strings.isNullOrEmpty(versionRule) || Strings.isNullOrEmpty(prefixSegementCount)){
            logger.error("Parse xml configuration headerRouter.xml error!");
            System.exit(0);
        }
        headerUrl.setPath(path);
        headerUrl.setRealPath(realPath);
        headerUrl.setHeaderKey(headerKey);
        headerUrl.setMicroServiceName(microServiceName);
        headerUrl.setVersionRule(versionRule);
        headerUrl.setPrefixSegmentCount(Integer.valueOf(prefixSegementCount));

        List<Element> elements = headUrlElement.elements();
        for (Element eachValueElement: elements) {
            if(!Strings.isNullOrEmpty(eachValueElement.getStringValue())){
                headerUrl.addHeaderValues(eachValueElement.getStringValue());
            }
        }
        Element paramRefElement = headUrlElement.element("paramRef");
        if(paramRefElement == null){
            return headerUrl;
        }
        headerUrl.setParamRef(true);
        ConfigParamValues configParamValues = GlobalRouterParams.GLOBAL_CONFIG_PARAM_VALUES.get(headerKey + "_" + microServiceName);
        if(configParamValues == null || configParamValues.getValues().isEmpty()){
            return headerUrl;
        }
        for (String eachValue : configParamValues.getValues()) {
            headerUrl.addHeaderValues(eachValue);
        }
        return headerUrl;
    }


}
