package com.je.gateway.router.model.headerRouter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * 请求头路由URL配置实体
 */
public class HeaderUrl implements Serializable {

    private String path;

    /**
     * 要真实转发的url，此优先级要高于router计算的url
     */
    private String realPath;

    private Pattern pattern;

    private String headerKey;

    private String microServiceName;

    private String versionRule;

    private int prefixSegmentCount;

    private boolean paramRef;

    /**
     * 此值主要用于在匹配后，暂存真实路由的值
     */
    private transient String routerValue;

    private List<String> headerValues = new ArrayList<>();

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
        this.pattern = Pattern.compile(path);
    }

    public String getRealPath() {
        return realPath;
    }

    public void setRealPath(String realPath) {
        this.realPath = realPath;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public String getHeaderKey() {
        return headerKey;
    }

    public void setHeaderKey(String headerKey) {
        this.headerKey = headerKey;
    }

    public List<String> getHeaderValues() {
        return headerValues;
    }

    public void setHeaderValues(List<String> headerValues) {
        this.headerValues = headerValues;
    }

    public String getMicroServiceName() {
        return microServiceName;
    }

    public void setMicroServiceName(String microServiceName) {
        this.microServiceName = microServiceName;
    }

    public String getVersionRule() {
        return versionRule;
    }

    public void setVersionRule(String versionRule) {
        this.versionRule = versionRule;
    }

    public int getPrefixSegmentCount() {
        return prefixSegmentCount;
    }

    public void setPrefixSegmentCount(int prefixSegmentCount) {
        this.prefixSegmentCount = prefixSegmentCount;
    }

    public boolean isParamRef() {
        return paramRef;
    }

    public void setParamRef(boolean paramRef) {
        this.paramRef = paramRef;
    }

    public void addHeaderValues(String value){
        this.headerValues.add(value);
    }

    public void removeHeaderValues(String value){
        this.headerValues.remove(value);
    }

    public String getRouterValue() {
        return routerValue;
    }

    public void setRouterValue(String routerValue) {
        this.routerValue = routerValue;
    }
}
