package com.je.gateway.router.dispatcher;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.je.gateway.security.PlatformInputSecurityValidatorFactory;
import com.je.gateway.util.DES;
import com.je.gateway.util.SpringContextHolder;
import io.vertx.core.MultiMap;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.lang.StringUtils;
import org.apache.servicecomb.provider.springmvc.reference.RestTemplateBuilder;
import org.apache.servicecomb.swagger.invocation.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 参数解密
 */
public class ParamDecryptDispatcher extends GatewayAbstractRestDispatcher {

    private static final Logger logger = LoggerFactory.getLogger(ParamDecryptDispatcher.class);
    /**
     * 匹配的URL
     */
    private static final String pattern = "/(.*)";
    /**
     * 加密参数名
     */
    private static final String PARAMS_KEY = "params-keys";
    /**
     * 默认密钥
     */
    private static final String DEFAULT_KEY1 = "JECLOUDDESCRYPT";
    /**
     * 默认加密字段
     */
    private static final String DEFAULT_PARAMS = "tableCode,funcCode,j_query,strData";

    private static final String DECRYPT_KEY = "systemDecryptCache";
    /**
     * 不需要解密的url请求
     */
    private String[] NOT_DECRYPT_URL = new String[]{"/je/rbac/wechat/checkCode", "/je/rbac/wechat/redirectToWechat", "/je/rbac/dingtalk/checkCode"
            , "/je/rbac/dingtalk/redirectToDingTalk", "/je/rbac/wechat/redirectToWechatHtml", "/je/rbac/wechat/getWeiXinPermissionsValidationConfig"};

    @Override
    public int getOrder() {
        return 1;
    }

    @Override
    public void init(Router router) {
        router.routeWithRegex(pattern)
                .failureHandler(this::onFailure)
                .handler(createBodyHandler())
                .handler(this::onRequest);
    }

    protected void onRequest(RoutingContext context) {
        RedisTemplate redisTemplate = SpringContextHolder.getBean("redisTemplate");
        MultiMap multiMap = context.request().params();
        Map<String, String[]> paramsMap = new HashMap<>();
        String uri = context.request().uri().substring(1, context.request().uri().indexOf("?") > 0 ?
                context.request().uri().indexOf("?") : context.request().uri().length());
        multiMap.forEach(entry -> {
            String key = entry.getKey();
            String value = entry.getValue();
            String[] values = value != null ? value.split(",") : new String[]{};
            paramsMap.put(key, values);
        });

        for (Map.Entry<String, String[]> eachEntry : paramsMap.entrySet()) {
            if (eachEntry.getValue().length == 0) {
                continue;
            }
            if (eachEntry.getValue()[0].equals(uri)) {
                continue;
            }
            Response response = PlatformInputSecurityValidatorFactory.validatorCharacter(eachEntry.getKey(), eachEntry.getValue()[0]);
            if (response != null) {
                context.request().response()
                        .putHeader("Content-Type", "text/plain; charset=utf-8")
                        .setStatusCode(HttpStatus.BAD_REQUEST.value())
                        .end(response.getResult().toString());
                return;
            }
        }

        if (context.fileUploads().size() != 0) {
            Response response = PlatformInputSecurityValidatorFactory.validatorFileUploads(context.fileUploads());
            if (response != null) {
                context.request().response()
                        .putHeader("Content-Type", "text/plain; charset=utf-8")
                        .setStatusCode(HttpStatus.BAD_REQUEST.value())
                        .end(response.getResult().toString());
                return;
            }
        }

        Object enableObj = redisTemplate.opsForHash().get(DECRYPT_KEY, "JE_SYS_ENCRYPT");
        if (!"1".equals(enableObj)) {
            context.next();
            return;
        }


        //获取混淆参数名
        String paramsKeys = context.request().getHeader(PARAMS_KEY);
        if (StringUtils.isBlank(paramsKeys)) {
            paramsKeys = context.request().getParam(PARAMS_KEY);
        }

        Object settingValue = redisTemplate.opsForHash().get(DECRYPT_KEY, "JE_SYS_ENCRYPT_FIELD");
        //获取配置的参数
        String configedFields = settingValue == null ? DEFAULT_PARAMS : settingValue.toString();
        List<String> paramList = Splitter.on(",").splitToList(configedFields);


        for (String key : paramsMap.keySet()) {
            if (paramList.contains(key) && paramsMap.get(key) != null && paramsMap.get(key).length > 0) {
                boolean allEmpty = true;
                for (String str : paramsMap.get(key)) {
                    if (str != null && !str.isEmpty()) {
                        allEmpty = false;
                        break;
                    }
                }
                if (allEmpty) {
                    continue;
                }
                logger.info(key + "没有加密，无法请求！");
                context.request().response()
                        .putHeader("Content-Type", "text/plain; charset=utf-8")
                        .setStatusCode(HttpStatus.BAD_REQUEST.value()).end("系统已启用传输加密，不允许明文传输！");
                return;
            }
        }

        if (Strings.isNullOrEmpty(paramsKeys)) {
            boolean flag = false;
            for (String eachParam : paramList) {
                if (context.request().params().contains(eachParam)) {
                    flag = true;
                    break;
                }
            }
            if (isNotDecryptUrl(context.request().uri())) {
                flag = false;
            }
            if (flag) {
                context.request().response()
                        .putHeader("Content-Type", "text/plain; charset=utf-8")
                        .setStatusCode(HttpStatus.BAD_REQUEST.value()).end("系统已启用传输加密，不允许明文传输！");
                return;
            }
            context.next();
            return;
        }

        Object key1 = redisTemplate.opsForHash().get(DECRYPT_KEY, "JE_SYS_ENCRYPT_KEY1");
        logger.error("--------------key1:" + key1);
        //获取密钥
        String key = key1 == null ? DEFAULT_KEY1 : key1.toString();

        //获取随机参数名
        List<String> keyList = Splitter.on(",").splitToList(paramsKeys);
        if (keyList.size() != paramList.size()) {
            context.request().response()
                    .putHeader("Content-Type", "text/plain; charset=utf-8")
                    .setStatusCode(HttpStatus.BAD_REQUEST.value()).end("前后端加密参数不一致，请重新加载页面！");
            executionTemplate();
            return;
        }

        if (paramList != null && keyList != null) {
            for (int i = 0; i < paramList.size(); i++) {
                String param = paramList.get(i);
                //获取参数值
                String value = context.request().getParam(keyList.get(i));
                //解密参数设置值
                if (!Strings.isNullOrEmpty(value)) {
                    String devalue = new DES(key).decrypt(value);
                    logger.debug("Add decrypt parameter {} value {} to request!", param, devalue);
                    context.request().params().add(param, devalue);
                }
            }
        }
        context.next();
    }

    private void executionTemplate() {
        String url = "cse://meta/je/meta/setting/loadEncryptSetting";
        RestTemplate restTemplate = RestTemplateBuilder.create();
        //发送请求
        restTemplate.getForObject(url, null);
    }

    public boolean isNotDecryptUrl(String uri) {
        String pathUri = uri.split("\\?")[0];
        return Arrays.stream(NOT_DECRYPT_URL).anyMatch(pathUri::equals);
    }

    public static void main(String[] args) {
        DES des = new DES("jlKcLuCUhA6kpPDrgqT");
        String enFuncCode = des.decrypt("6ba3f10ced21cfab25e576cf548b03ae0c524964eed95394");
        String enTableCode = des.decrypt("6ee8881650996132963119685793a2fe25e576cf548b03ae07afc36cca77dbba81cceca4ffc28455cef0c8d58f989ce4c68cf3090a23df626cc694cbe815412ec973f155037112f896519db875868e4c963119685793a2fe064e2e4207b3a5b071d1bd0d6f1aaf32255f54f038a974cdd9e3978c9cfaa969");
        String enJquery = des.encryption("{}");
        System.out.println(enFuncCode);
        System.out.println(enTableCode);
        System.out.println(enJquery);
    }

}
