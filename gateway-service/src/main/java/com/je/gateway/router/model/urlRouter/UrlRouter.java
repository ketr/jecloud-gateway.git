package com.je.gateway.router.model.urlRouter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * UrlRouter配置实体
 */
public class UrlRouter implements Serializable {

    private String path;
    private Pattern pattern = Pattern.compile("/(.*)");
    private boolean enable = false;
    private List<UrlDetail> urlDetails = new ArrayList<>();

    public UrlRouter() {
    }

    public UrlRouter(String pattern) {
        this.path = pattern;
        this.pattern = Pattern.compile(pattern);
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
        this.pattern = Pattern.compile(path);
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public List<UrlDetail> getUrlDetails() {
        return urlDetails;
    }

    public void setUrlDetails(List<UrlDetail> urlDetails) {
        this.urlDetails = urlDetails;
    }

    public void addUrl(UrlDetail urlDetail){
        this.urlDetails.add(urlDetail);
    }

    public void removeUrl(UrlDetail urlDetail){
        this.urlDetails.remove(urlDetail);
    }


}
