package com.je.gateway.router.model.urlRouter;

import java.io.Serializable;
import java.util.regex.Pattern;

public class UrlDetail implements Serializable {

    private String path;
    /**
     * 要真实转发的url，此优先级要高于router计算的url
     */
    private String realPath;
    private Pattern pattern;

    private String microServiceName;

    private String versionRule;

    private int prefixSegmentCount;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
        this.pattern = Pattern.compile(path);
    }

    public String getRealPath() {
        return realPath;
    }

    public void setRealPath(String realPath) {
        this.realPath = realPath;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public String getMicroServiceName() {
        return microServiceName;
    }

    public void setMicroServiceName(String microServiceName) {
        this.microServiceName = microServiceName;
    }

    public String getVersionRule() {
        return versionRule;
    }

    public void setVersionRule(String versionRule) {
        this.versionRule = versionRule;
    }

    public int getPrefixSegmentCount() {
        return prefixSegmentCount;
    }

    public void setPrefixSegmentCount(int prefixSegmentCount) {
        this.prefixSegmentCount = prefixSegmentCount;
    }

}
