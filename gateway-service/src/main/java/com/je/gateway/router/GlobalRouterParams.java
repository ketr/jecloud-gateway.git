package com.je.gateway.router;

import com.je.gateway.router.loader.ConfigParamValuesResourceLoader;
import com.je.gateway.router.loader.UrlHeaderRouterResourceLoader;
import com.je.gateway.router.loader.UrlParamRouterResourceLoader;
import com.je.gateway.router.loader.UrlRouterResourceLoader;
import com.je.gateway.router.model.headerRouter.HeaderRouter;
import com.je.gateway.router.model.paramRouter.ParamRouter;
import com.je.gateway.router.model.paramValue.ConfigParamValues;
import com.je.gateway.xml.ConfigResourceLoader;
import com.je.gateway.router.model.urlRouter.UrlRouter;
import java.util.HashMap;
import java.util.Map;

/**
 * @program: jecloud-gateway
 * @author: LIULJ
 * @create: 2021/7/4
 * @description:
 */
public class GlobalRouterParams {

    /**
     * 缓存同步KEY
     */
    public static final String GLOBAL_ROUTER_SYNC_KEY = "routerSyncCache";
    /**
     * 同步KEY存活时间，默认为60s
     */
    public static final Long GLOBAL_ROUTER_SYNC_EXPIRE_TIME = 60L;


    /**
     * 资源参数加载器
     */
    private static final ConfigResourceLoader<Map<String, ConfigParamValues>> configParamValuesResourceLoader = new ConfigParamValuesResourceLoader();
    /**
     * 请求头路由加载器
     */
    private static final ConfigResourceLoader<HeaderRouter> xmlUrlHeaderConfigResourceLoader = new UrlHeaderRouterResourceLoader();
    /**
     * 请求参数路由加载器
     */
    private static final ConfigResourceLoader<ParamRouter> xmlUrlParamsConfigResourceLoader = new UrlParamRouterResourceLoader();
    /**
     * URL请求参数路由加载器
     */
    private static final ConfigResourceLoader<UrlRouter> xmlUrlConfigResourceLoader = new UrlRouterResourceLoader();
    /**
     * 全局请求头路由
     */
    public static HeaderRouter GLOBAL_HEADER_ROUTER;
    /**
     * 全局消息参数路由
     */
    public static ParamRouter GLOBAL_PARAM_ROUTER;
    /**
     * 全局Url路由
     */
    public static UrlRouter GLOBAL_URL_ROUTER;
    /**
     * 全局配置化参数，用于路由
     */
    public static Map<String,ConfigParamValues> GLOBAL_CONFIG_PARAM_VALUES = new HashMap<>();

    public static void loadConfigValues(){
        GLOBAL_CONFIG_PARAM_VALUES = configParamValuesResourceLoader.load();
    }

    public static void loadUrlHeaderValues(){
        GLOBAL_HEADER_ROUTER = xmlUrlHeaderConfigResourceLoader.load();

    }

    public static void loadUrlParamValues(){
        GLOBAL_PARAM_ROUTER = xmlUrlParamsConfigResourceLoader.load();
    }

    public static void loadUrlValues(){
        GLOBAL_URL_ROUTER = xmlUrlConfigResourceLoader.load();
    }

    public static void reload(){
        loadConfigValues();
        loadUrlHeaderValues();
        loadUrlParamValues();
        loadUrlValues();
    }

}
