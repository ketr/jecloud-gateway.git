package com.je.gateway.router.loader;

import com.je.gateway.router.model.paramValue.ConfigValues;
import com.je.gateway.util.SpringContextHolder;
import com.je.gateway.xml.ConfigResourceLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 动态路由参数资源加载器
 */
public class DynamicConfigParamValuesResourceLoader implements ConfigResourceLoader<Map<String, ConfigValues>> {

    private static final Logger logger = LoggerFactory.getLogger(DynamicConfigParamValuesResourceLoader.class);

    @Override
    public Map<String, ConfigValues> load() {
        Map<String,ConfigValues> configParamValuesMap = new HashMap<>();
        try {
            RedisTemplate<String,Object> redisTemplate = SpringContextHolder.getBean(RedisTemplate.class);
            Object cacheObj = redisTemplate.opsForValue().get("routerCache");
            if(cacheObj == null){
                return configParamValuesMap;
            }
            List<Map<String,Object>> paramsList = (List<Map<String, Object>>) cacheObj;
            ConfigValues configValues;
            for (Map<String,Object> eachParam : paramsList) {
                configValues = new ConfigValues();
                configValues.setName(eachParam.get("PARAMS_CODE").toString());
                configValues.setType(eachParam.get("PARAMS_TYPE").toString());
                if(eachParam.containsKey("values")){
                    List<String> values = (List<String>) eachParam.get("values");
                    configValues.addAllValue(values);
                }
                logger.info("Add router value type {} code {}!",configValues.getType(),configValues.getName());
                configParamValuesMap.put(configValues.getName() + "_" + configValues.getType(),configValues);
            }
        }catch (Exception e){
            e.printStackTrace();
            logger.error(e.getMessage(),e);
        }

        return configParamValuesMap;
    }

}
