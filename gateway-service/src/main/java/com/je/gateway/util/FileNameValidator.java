package com.je.gateway.util;

import java.util.regex.Pattern;

public class FileNameValidator {

    // 改进的正则：校验文件名是否含有潜在的 XSS 攻击字符
    private static final String XSS_PATTERN = "[<>\"'&]";

    /**
     * 检查文件名是否包含潜在的 XSS 攻击内容
     *
     * @param fileName 文件名
     * @return 是否包含 XSS 攻击内容
     */
    public static boolean isValidFileName(String fileName) {
        if (fileName == null || fileName.trim().isEmpty()) {
            return true; // 空文件名不包含 XSS
        }
        return !(Pattern.compile(XSS_PATTERN).matcher(fileName).find());
    }

    /**
     * 对文件名中的特殊字符进行安全转义
     *
     * @param fileName 文件名
     * @return 转义后的文件名
     */
    public static String escapeFileName(String fileName) {
        if (fileName == null) {
            return null;
        }
        return fileName.replaceAll("<", "&lt;")
                .replaceAll(">", "&gt;")
                .replaceAll("\"", "&quot;")
                .replaceAll("'", "&#39;")
                .replaceAll("&", "&amp;");
    }

    public static void main(String[] args) {
        String safeFileName = "新建 DOCX 文档 (2).pdf";
        String safeFileName2 = "【13123123123.pdf】";
        String xssFileName = "file<script>.pdf";

        System.out.println("Safe File: " + isValidFileName(safeFileName)); // false
        System.out.println("XSS File: " + isValidFileName(xssFileName));   // true
        System.out.println("XSS File: " + isValidFileName(safeFileName2));   // true
        System.out.println("Escaped: " + escapeFileName(xssFileName)); // file&lt;script&gt;.pdf
    }
}
