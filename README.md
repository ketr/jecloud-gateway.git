# 网关服务项目

## 项目简介

JECloud 网关管理模块，用于管理接入微服务体系的动态请求与路由，默认已实现URL路由、Header路由、参数路由、产品路由等。
其主要包括如下功能：

- 接口鉴权
- 接口限流
- 服务路由
- IP限制管理

## 环境依赖

* jdk1.8
* maven
* 请使用官方仓库(http://maven.jepaas.com)，暂不同步jar到中央仓库。


> Maven安装 (http://maven.apache.org/download.cgi)

> Maven学习，请参考[maven-基础](docs/mannual/maven-基础.md)

## 服务模块介绍

- aggregate-tomcat： 服务聚合模块，使用SpringBoot插件构造应用Jar，默认使用tomcat。
- gateway-service: 动态网关核心服务实现。

## 编译部署

使用maven生成对应环境的jar包，例: 生产环境打包
``` shell
//开发环境打包
mvn clean package -Pdev -Dmaven.test.skip=true
//本地开发环境打包
mvn clean package -Plocal -Dmaven.test.skip=true
//测试环境打包
mvn clean package -Ptest -Dmaven.test.skip=true
//生产环境打包
mvn clean package -Pprod -Dmaven.test.skip=true
```

## 开源协议

- [MIT](./LICENSE)
- [平台证书补充协议](./SUPPLEMENTAL_LICENSE.md)

## JECloud主目录
[JECloud 微服务架构低代码平台（点击了解更多）](https://gitee.com/ketr/jecloud.git)